//
//  MemberPartyTableViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/25/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class MemberPartyTableViewController: UITableViewController {

    //MARK: - Properties
    var stateController: StateController!
    var member: Member!
    
    private let cellId = "cellId"
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = member.username
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.allowsMultipleSelection = true
    }
    
    //MARK: - UITableView DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateController.parties.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.selectionStyle = .none
        guard let party = stateController?.parties[indexPath.row] else {return UITableViewCell()}
        cell.textLabel?.text = party.name
        
        if isMemberInvited(partyIndex: indexPath.row) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    //MARK: - UITableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var party = stateController.parties[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        if isMemberInvited(partyIndex: indexPath.row) {
            party.members.removeAll(where: {$0.id == member.id})
            stateController.update(party: party)
            cell?.accessoryType = .none
        } else {
            party.members.append(member)
            stateController.update(party: party)
            cell?.accessoryType = .checkmark
        }
    }
    
    //MARK: - Helper methods
    private func isMemberInvited(partyIndex: Int) -> Bool {
        let members = stateController.parties[partyIndex].members
        return members.contains(where: {$0.id == member.id})
    }
}
