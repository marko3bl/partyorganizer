//
//  PartiesViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol PartyProvider: class {
    func addNewParty()
}

class PartiesViewController: UIViewController {
    
    //MARK: - Properties
    var stateController: StateController!
    
    var parties: [Party] {
        get {
            return stateController.parties
        }
    }
    
    private let cellId = "cellId"
    
    //MARK: - View Properites
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isHidden = true
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    
    private lazy var noPartyView: NoPartyView = {
        let view = NoPartyView()
        view.partyDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Parties"
        view.backgroundColor = .white
        setupAddButton()
        setupNoPartyView()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        toggleViews()
        tableView.reloadData()
    }
    
    //MARK: - Helper methods
    //Setup bar button item - Add button
    private func setupAddButton() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped))
        navigationItem.rightBarButtonItem = addButton
    }
    
    private func setupNoPartyView() {
        view.addSubview(noPartyView)
        noPartyView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noPartyView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        noPartyView.widthAnchor.constraint(equalToConstant: 300).isActive = true
        noPartyView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        tableView.register(PartyCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func showPartVC(forParty: Party? = nil) {
        let partyVC = PartyTableViewController()
        partyVC.stateController = stateController
        
        partyVC.parties = parties
        
        if forParty != nil {
            partyVC.selectedParty = forParty
        }
        
        let navController = UINavigationController(rootViewController: partyVC)
        
        present(navController, animated: true, completion: nil)
    }
    
    // Create party method
    private func createParty() {
        showPartVC()
    }
    
    // check is there any party created
    private func isAnyPartyCreated() -> Bool {
        return stateController.parties.isEmpty
        
    }
    
    //toggle between noPartyView and Table View
    private func toggleViews() {
        noPartyView.isHidden = !isAnyPartyCreated()
        tableView.isHidden = isAnyPartyCreated()
    }
    
    //MARK: - Implement Add button method
    @objc private func addButtonTapped() {
        createParty()
    }
}

//MARK: - Implement PartyProvider protocol
extension PartiesViewController: PartyProvider {
    
    func addNewParty() {
        createParty()
    }
}

//MARK: - UITableView DataSource
extension PartiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parties.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PartyCell
        let party = parties[indexPath.row]
        cell.party = party
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            let party = self.parties[indexPath.row]
            tableView.beginUpdates()
            self.stateController.remove(party: party)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            self.toggleViews()
            tableView.endUpdates()

        }
        
        return [deleteAction]
    }
}

//MARK: - UITableView Delegate
extension PartiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let party = parties[indexPath.row]
        showPartVC(forParty: party)
    }
}
