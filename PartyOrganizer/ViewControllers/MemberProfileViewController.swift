//
//  MemberProfileViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol AddPartyDelegate: class {
    func addToParty()
}

class MemberProfileViewController: UIViewController {
    
    //MARK: - Properties
    var memberProfile: Member! {
        didSet {
            memberProfileView.updateUIFor(memberProfile)
        }
    }
    
    var stateController: StateController!
    
    //MARK: - View properties
    private lazy var memberProfileView: MemberProfileVew = {
        let view = MemberProfileVew()
        view.addToPartyDelegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Profile"
        setupBarButtons()
        setupProfileView()
    }

    //MARK: - Helper methods
    private func setupBarButtons() {
        let callButton = UIBarButtonItem(title: "Call", style: .plain, target: self, action: #selector(callButtonTapped))
        let backButton = UIBarButtonItem(image: UIImage(named: "backIcon"), style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.rightBarButtonItem = callButton
        navigationItem.leftBarButtonItem = backButton
    }
    
    private func setupProfileView() {
        view.addSubview(memberProfileView)
        memberProfileView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        memberProfileView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        memberProfileView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        memberProfileView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
    }
    
    //MARK: - UIBarButton actions
    @objc private func callButtonTapped() {
        print("Call button tapped")
    }
    
    @objc private func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }

}

//MARK: - Implement AddPartyDelegate
extension MemberProfileViewController: AddPartyDelegate {
    
    func addToParty() {
        let memberPartyVC = MemberPartyTableViewController()
        memberPartyVC.stateController = stateController
        memberPartyVC.member = memberProfile
        navigationController?.pushViewController(memberPartyVC, animated: true)
    }
    
}
