//
//  CustomTabController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class CustomTabController: UITabBarController {
    
    let stateController: StateController!
    
    init(stateController: StateController) {
        self.stateController = stateController
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabController()
    }
    
    private func setupTabController() {
        
        let partyVC = PartiesViewController()
        partyVC.stateController = stateController
        
        let navControllerForPartyVC = UINavigationController(rootViewController: partyVC)
        partyVC.tabBarItem = UITabBarItem(title: "Parties", image: UIImage(named: "partyIcon"), tag: 0)
        
        let memberVC = MemberViewController()
        memberVC.stateController = stateController
        
        let navControllerForMemberVC = UINavigationController(rootViewController: memberVC)
        memberVC.tabBarItem = UITabBarItem(title: "Members", image: UIImage(named: "memberIcon"), tag: 1)
        
        viewControllers = [navControllerForPartyVC, navControllerForMemberVC]
    }

}
