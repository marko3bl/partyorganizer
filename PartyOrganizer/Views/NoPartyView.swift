//
//  NoDataView.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class NoPartyView: UIView {
    
    //MARK: - Party delegate
    weak var partyDelegate: PartyProvider?
    
    //MARK: - Properties
    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "You have no party.\nCreate some!"
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var addButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        let title = NSAttributedString(string: "Create party",
                                       attributes: [
                                        .font: UIFont.systemFont(ofSize: 20, weight: .semibold),
                                        .foregroundColor: UIColor.white
                                        ])
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(createPartyButtonTapped), for: .touchUpInside)
        button.backgroundColor = UIColor.blueButtonColor
        button.layer.cornerRadius = 10
        return button
    }()

    //MARK: - Create object
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStackView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helper methods
    private func setupStackView() {
        
        addSubview(descriptionLabel)
        addSubview(addButton)
        
        descriptionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        descriptionLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        addButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        addButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        addButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    //MARK: - Create party method
    @objc private func createPartyButtonTapped() {
        partyDelegate?.addNewParty()
    }
    
    
}
