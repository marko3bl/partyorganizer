//
//  DescriptionLabelCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class DescriptionTextViewCell: UITableViewCell {
    
    var descriptionText: String {
        get {
            return textView.text
        }
        
        set {
            textView.text = newValue
        }
    }
    
    private let textView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isEditable = true
        tv.textColor = UIColor.gray
        tv.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        tv.isScrollEnabled = false
        return tv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        addSubview(textView)
        textView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        textView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        
    }
}
