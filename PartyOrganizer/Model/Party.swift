//
//  Party.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

struct Party: Codable {
    let id: String
    var name: String
    var partyDate: Date
    var description = ""
    var members: [Member]
}
