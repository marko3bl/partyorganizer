//
//  PartyTableViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class PartyTableViewController: UITableViewController {
    
    //MARK: - Properties
    var selectedParty: Party?
    var invitedMembers: [Member]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    //MARK: - Default New Party
    var newParty = Party(id: UUID().uuidString, name: "", partyDate: Date(), description: "", members: [])
    
     var parties: [Party]!
    
    //MARK: - State Controller
    var stateController: StateController!
    
    //MARK: - Static TableView Cells
    private let partyNameCell = PartyNameCell()
    private let partyDateCell = PartyDateCell()
    private let membersCell = MembersStaticCell()
    private let tableViewCell = PartyMembersCell()
    private let descriptionCell = DescriptionStaticCell()
    private let descriptionTextViewCell = DescriptionTextViewCell()
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        setupNavigationItems()
    }
    
    //MARK: - Helper methods
    
    private func calculateTextHeight(text: String?) -> CGFloat {
        guard let textString = text else {return 200}
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let rect = NSString(string: textString).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)], context: nil)
        return rect.height
    }
    
    private func setupNavigationItems() {
        title = "Party"
        let homeButton = UIBarButtonItem(image: UIImage(named: "backIcon"), style: .plain, target: self, action: #selector(backButtonTapped))
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonTapped))
        navigationItem.leftBarButtonItem = homeButton
        navigationItem.rightBarButtonItem = saveButton
    }

    //MARK: - UIBarButton actions
    @objc func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func saveButtonTapped() {
        guard let partyName = partyNameCell.textField.text, let _ = partyDateCell.textField.text, !descriptionTextViewCell.descriptionText.isEmpty else {
                showAlert(with: "Please fill all fields!")
                return
        }
        
        let name = partyName
        let date = partyDateCell.date
        let descriptionText = descriptionTextViewCell.descriptionText

        if selectedParty != nil {
            selectedParty?.name = name
            selectedParty?.partyDate = date
            selectedParty?.description = descriptionText

            stateController.update(party: selectedParty!)
            
        } else {
            newParty.name = name
            newParty.partyDate = date
            newParty.description = descriptionText
            
            stateController.add(party: newParty)
        }

        dismiss(animated: true, completion: nil)
        
    }

    //MARK: - UITableView DataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            partyNameCell.textField.text = selectedParty?.name
            return partyNameCell
        case 1:
            partyDateCell.textField.text = selectedParty?.partyDate.convertDateAndTimeToString()
            return partyDateCell
        case 2:
            membersCell.membersCount = selectedParty?.members.count ?? newParty.members.count
            return membersCell
        case 3:
            if selectedParty != nil {
                tableViewCell.members = selectedParty!.members
            } else {
                tableViewCell.members = newParty.members
            }
            tableViewCell.invitedMembersCountDelegate = self
            return tableViewCell
        case 4: return descriptionCell
        case 5:
            descriptionTextViewCell.descriptionText = selectedParty?.description ?? ""
            return descriptionTextViewCell
        default:
            return UITableViewCell()
        }
    }
    
    //MARK: - UITableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 2:
            let memberPreviewVC = MemberPreviewViewController()
            memberPreviewVC.memberInvitationDelegate = self
            memberPreviewVC.stateController = stateController
            
            if selectedParty != nil {
                stateController.update(party: selectedParty!)
                memberPreviewVC.party = selectedParty
            } else {
                stateController.update(party: newParty)
                memberPreviewVC.party = newParty
            }
            
            navigationController?.pushViewController(memberPreviewVC, animated: true)
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 3: return 250
        case 5:
            let height = calculateTextHeight(text: selectedParty?.description)
            return tableView.systemLayoutSizeFitting(CGSize(width: view.frame.width, height: height)).height + 32
        default: return 50
        }
    }

}

extension PartyTableViewController: MemberInvitationProvider {
    
    func saveInvitedMembersTo(party: Party) {
        if selectedParty?.id == party.id {
            selectedParty = party
        } else {
            newParty = party
        }
        tableView.reloadData()
    }

}

extension PartyTableViewController: InvitedMembersCountProvider {
    func newInvited(members: [Member]) {
        if selectedParty != nil {
            selectedParty!.members = members
        } else {
            newParty.members = members
        }
    }
    
    func invitedMembers(count: Int) {
        membersCell.membersCount = count
    }
}
