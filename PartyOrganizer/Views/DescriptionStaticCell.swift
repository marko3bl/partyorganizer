//
//  DescriptionStaticCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class DescriptionStaticCell: BasicStaticCell {


    override func setupViews() {
        super.setupViews()
        selectionStyle = .none
        nameLabel.text = "Description:"
        textField.removeFromSuperview()
        backgroundColor = UIColor.blueButtonColor
        nameLabel.textColor = .white
    }

}
