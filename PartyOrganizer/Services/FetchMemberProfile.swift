//
//  FetchMemberProfile.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

protocol MemberProfileProvider: class {
    func fetchMemberProfile(from urlString: String, completion: @escaping (MembersFromServer?) -> ())
}

class FetchMemberProfile: MemberProfileProvider {
    
    
    func fetchMemberProfile(from urlString: String, completion: @escaping (MembersFromServer?) -> ()) {
        
        guard let url = URL(string: urlString) else {
            completion(nil)
            print("URL is not correct!")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            
            if error != nil {
                print("Faild to fetch data: \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let jsonData = data else {
                completion(nil)
                return
            }
            
            do {
                
                let members = try JSONDecoder().decode(MembersFromServer.self, from: jsonData)
                
                DispatchQueue.main.async {
                    completion(members)
                }
                
            } catch let err {
                print("Faild to decode data from jsonData: \(err.localizedDescription)")
                completion(nil)
            }
        
        }.resume()
    }
    
}
