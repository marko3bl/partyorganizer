//
//  MembersFromServer.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

struct MembersFromServer: Decodable {
    let profiles: [Member]
}
