//
//  MemberViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class MemberViewController: UIViewController {
    
    //MARK: - Member profile provider
    var memberProfileProvider: MemberProfileProvider!

    var stateController: StateController!
    
    //MARK: - Private Properties
    private let cellId = "memberCell"
    private let urlString = "http://api-coin.quantox.tech/profiles.json"
    
    private var members: [Member]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    //MARK: - View Properties
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.register(MemberCell.self, forCellReuseIdentifier: cellId)
        tv.separatorColor = UIColor.lightGray
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Members"
        view.backgroundColor = .white
        
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        memberProfileProvider = FetchMemberProfile()
        memberProfileProvider.fetchMemberProfile(from: urlString) { (members) in
            guard let profiles = members?.profiles else {return}
            self.members = profiles
        }
    }
    
    //MARK: - Helper methods
    //Setup table view
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

}

//MARK: UITableView DataSource
extension MemberViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MemberCell
        guard let member = members?[indexPath.row] else {return UITableViewCell()}
        cell.member = member
        return cell
    }
}

//MARK: - UITableView Delegate
extension MemberViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let member = members?[indexPath.row] else {return}
        let memberProfileVC = MemberProfileViewController()
        memberProfileVC.memberProfile = member
        memberProfileVC.stateController = stateController
        let navController = UINavigationController(rootViewController: memberProfileVC)
        
        present(navController, animated: true, completion: nil)
        
    }
    
}
