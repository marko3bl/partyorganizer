//
//  PartyMembersCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol InvitedMembersCountProvider: class {
    func invitedMembers(count: Int)
    func newInvited(members: [Member])
}

class PartyMembersCell: UITableViewCell {
    
    var members: [Member] = [] {
        didSet {
            memberTableView.reloadData()

        }
    }
    
    weak var invitedMembersCountDelegate: InvitedMembersCountProvider?
    
    private lazy var memberTableView: UITableView = {
        let tv = UITableView(frame: .zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorStyle = .none
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    
    private let cellId = "cellId"


    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTableView() {
        addSubview(memberTableView)
        
        memberTableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        memberTableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        memberTableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        memberTableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        memberTableView.tableFooterView = UIView()
        memberTableView.estimatedRowHeight = 100
        memberTableView.rowHeight = UITableView.automaticDimension
        
        memberTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }

}

extension PartyMembersCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.backgroundColor = UIColor.groupTableViewBackground
        cell.selectionStyle = .none
        let member = members[indexPath.row]
        cell.textLabel?.text = member.username
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            tableView.beginUpdates()
            self.members.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            tableView.endUpdates()
            
            self.invitedMembersCountDelegate?.invitedMembers(count: self.members.count)
            self.invitedMembersCountDelegate?.newInvited(members: self.members)
        }
        
        return [delete]
    }
}

extension PartyMembersCell: UITableViewDelegate {
    
}


