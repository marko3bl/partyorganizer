//
//  Member.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

struct Member: Codable {
    let id: Int
    let username: String
    let cell: String
    let photo: String
    let email: String
    let gender: String
    let aboutMe: String
}
