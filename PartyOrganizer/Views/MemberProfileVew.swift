//
//  MemberProfileVew.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class MemberProfileVew: UIView {
    
    //MARK: - Add to party delegate
    weak var addToPartyDelegate: AddPartyDelegate?
    
    //MARK: - View properties
    private let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.layer.cornerRadius = 75
        iv.layer.masksToBounds = true
        iv.layer.borderWidth = 2
        iv.layer.borderColor = UIColor.gray.cgColor
        return iv
    }()
    
    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Full name:"
        return label
    }()
    
    private let genderLabel: UILabel = {
        let label = UILabel()
        label.text = "Gender:"
        return label
    }()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.text = "Email:"
        return label
    }()
    
    private let aboutLabel: UILabel = {
        let label = UILabel()
        label.text = "About:"
        return label
    }()
    
    private let textView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.textColor = UIColor.gray
        tv.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        tv.isEditable = false
        tv.textAlignment = .justified
        return tv
    }()
    
    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.distribution = .fill
        sv.axis = .vertical
        sv.alignment = .fill
        sv.spacing = 20
        return sv
    }()
    
    private lazy var addToPartyButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        let title = NSAttributedString(string: "Add to party",
                                       attributes: [
                                        .font: UIFont.systemFont(ofSize: 20, weight: .semibold),
                                        .foregroundColor: UIColor.white
            ])
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(addToPartyButtonTapped), for: .touchUpInside)
        button.backgroundColor = UIColor.blueButtonColor
        button.layer.cornerRadius = 10
        return button
    }()
    
    //MARK: - Update UI
    func updateUIFor(_ member: Member) {
        profileImageView.loadImageUsingCacheWith(urlString: member.photo)
        fullNameLabel.text = "Full name: \(member.username)"
        genderLabel.text = "Gender: \(member.gender)"
        emailLabel.text = "Email: \(member.email)"
        textView.text = member.aboutMe
    }

    //MARK: - Create object
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(profileImageView)
        addSubview(stackView)
        addSubview(textView)
        addSubview(addToPartyButton)
        
        stackView.addArrangedSubview(fullNameLabel)
        stackView.addArrangedSubview(genderLabel)
        stackView.addArrangedSubview(emailLabel)
        stackView.addArrangedSubview(aboutLabel)
        
        profileImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        stackView.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 16).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 16).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        textView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 5).isActive = true
        textView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        textView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        textView.bottomAnchor.constraint(equalTo: addToPartyButton.topAnchor, constant: 8).isActive = true

        addToPartyButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32).isActive = true
        addToPartyButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        addToPartyButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        addToPartyButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Add To Party method
    @objc private func addToPartyButtonTapped() {
        addToPartyDelegate?.addToParty()
    }

}
