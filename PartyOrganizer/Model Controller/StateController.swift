//
//  StateController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

class StateController {
    
    fileprivate let storageController: StorageController
    
    var parties: [Party]
    
    init(storageController: StorageController) {
        self.storageController = storageController
        parties = storageController.fetchParties()
    }
    
    func add(party: Party) {
        parties.append(party)
        storageController.save(parties)
    }
    
    func update(party: Party) {
        for (index, storedParty) in parties.enumerated() {
            guard storedParty.id == party.id else {continue}
            parties[index] = party
            storageController.save(parties)
            break
        }
    }
    
    func remove(party: Party) {
        for (index, storedParty) in parties.enumerated() {
            guard storedParty.id == party.id else {continue}
            parties.remove(at: index)
            storageController.save(parties)
        }
    }
}
