//
//  PartyDateCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class PartyDateCell: BasicStaticCell {
    
    var date: Date {
        return datePicker.date
    }

    private let datePicker = UIDatePicker()

    override func setupViews() {
        super.setupViews()
        selectionStyle = .none
        nameLabel.text = "Party date"
        textField.placeholder = "Start date and time"
        
        showDatePicker()
    }

    private func showDatePicker() {
        
        //Set toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        //Setup done and cancel button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonTapped))
        
        toolBar.setItems([doneButton, spaceButton, cancelButton], animated: true)

        textField.inputAccessoryView = toolBar
        textField.inputView = datePicker
    }
    
    @objc private func doneButtonTapped() {
        textField.text = datePicker.date.convertDateAndTimeToString()
        self.endEditing(true)
    }
    
    @objc private func cancelButtonTapped() {
        self.endEditing(true)
    }
    
}
