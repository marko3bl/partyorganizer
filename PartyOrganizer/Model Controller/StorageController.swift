//
//  StorageController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/25/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation

class StorageController {
    
    fileprivate let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    fileprivate var partiesFileURL: URL {
        return documentsDirectoryURL.appendingPathComponent("Parties").appendingPathExtension("plist")
    }
    
    func save(_ parties: [Party]) {
        let partyEncoder = PropertyListEncoder()
        
        do {
            
            let partiesPlist = try partyEncoder.encode(parties)
            
            do {
                
                try partiesPlist.write(to: partiesFileURL, options: .noFileProtection)
                
            } catch {
                print("Faild to write data to disk")
            }
            
        } catch {
            print("Faild to encode data: \(error.localizedDescription)")
        }
        
    }
    
    func fetchParties() -> [Party] {
        
        let partyDecoder = PropertyListDecoder()
        
        do {
            let data = try Data(contentsOf: partiesFileURL)
            return try partyDecoder.decode([Party].self, from: data)
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
    
}
