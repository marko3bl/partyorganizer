//
//  MemberPreviewViewController.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol MemberInvitationProvider: class {
    func saveInvitedMembersTo(party: Party)
}

class MemberPreviewViewController: UIViewController {
    
    //MARK: - Properties
    var stateController: StateController!
    var party: Party!
    
    //MARK: - Member profile provider
    var memberProfileProvider: MemberProfileProvider!

    weak var memberInvitationDelegate: MemberInvitationProvider?
    
    
    //MARK: - Private Properties
    private let cellId = "memberPreviewCell"
    private let urlString = "http://api-coin.quantox.tech/profiles.json"
    
    private var members: [Member]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    //MARK: - View Properties
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.register(MemberPreviewCell.self, forCellReuseIdentifier: cellId)
        tv.separatorColor = UIColor.lightGray
        tv.allowsMultipleSelection = true
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Members"
        view.backgroundColor = .white
        
        setupTableView()
        setupNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        memberProfileProvider = FetchMemberProfile()
        memberProfileProvider.fetchMemberProfile(from: urlString) { (members) in
            guard let profiles = members?.profiles else {return}
            self.members = profiles
        }
    }
    
    //MARK: - Helper methods
    //Setup table view
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func setupNavigationItem() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonTapped))
        navigationItem.rightBarButtonItem = saveButton
    }

    private func isMemberInvited(partyIndex: Int) -> Bool {
        guard let member = members?[partyIndex] else {return false}
        guard let storedParty = stateController.parties.filter({$0.id == party.id}).first else {return false}
        return storedParty.members.contains(where: {$0.id == member.id})
    }
    
    //MARK: - Save changes method
    @objc func saveButtonTapped() {
        memberInvitationDelegate?.saveInvitedMembersTo(party: party)
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITableView DataSource
extension MemberPreviewViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MemberPreviewCell
        cell.delegate = self
        let member = members?[indexPath.row]
        cell.member = member
        
        if isMemberInvited(partyIndex: indexPath.row) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
}

//MARK: - UITableView Delegate
extension MemberPreviewViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let member = members?[indexPath.row] else {return}
        let cell = tableView.cellForRow(at: indexPath)
        
        if isMemberInvited(partyIndex: indexPath.row) {
            party.members.removeAll(where: {$0.id == member.id})
            stateController.update(party: party)
            cell?.accessoryType = .none
        } else {
            party.members.append(member)
            stateController.update(party: party)
            cell?.accessoryType = .checkmark
        }
    }
}

//MARK: - Implement MemberProfileDelegate protocol
extension MemberPreviewViewController: MemberProfileDelegate {
    func showMemberProfile(member: Member) {
        let memberProfileVC = MemberProfileViewController()
        memberProfileVC.memberProfile = member
        memberProfileVC.stateController = stateController
        let navController = UINavigationController(rootViewController: memberProfileVC)
        present(navController, animated: true, completion: nil)
    }
}
