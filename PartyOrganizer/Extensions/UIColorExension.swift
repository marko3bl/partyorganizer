//
//  UIColorExension.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/25/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var blueButtonColor: UIColor {
        return UIColor(red: 52/255, green: 152/255, blue: 219/255, alpha: 1)
    }
}
