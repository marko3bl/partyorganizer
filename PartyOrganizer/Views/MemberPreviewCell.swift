//
//  MemberPreviewCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol MemberProfileDelegate: class {
    func showMemberProfile(member: Member)
}

class MemberPreviewCell: MemberCell {
    
    weak var delegate: MemberProfileDelegate?

    override func setupViews() {
        super.setupViews()
        
        selectionStyle = .none
        
        profileImageView.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(gestureRegocnizer:)))
        profileImageView.addGestureRecognizer(tapGesture)
        
    }

    @objc func profileImageTapped(gestureRegocnizer: UITapGestureRecognizer) {
        guard let mem = member else {return}
        delegate?.showMemberProfile(member: mem)
    }
}
