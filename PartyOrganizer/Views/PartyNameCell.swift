//
//  PartyNameCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/23/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class PartyNameCell: BasicStaticCell {


    override func setupViews() {
        super.setupViews()
        selectionStyle = .none
        nameLabel.text = "Name"
        textField.placeholder = "Party name"
    }

}
