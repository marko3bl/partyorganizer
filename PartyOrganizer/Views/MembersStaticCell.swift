//
//  MembersStaticCell.swift
//  PartyOrganizer
//
//  Created by Marko Tribl on 2/24/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class MembersStaticCell: BasicStaticCell {

    var membersCount: Int = 0 {
        didSet {
            nameLabel.text = "Members (\(membersCount)):"
        }
    }

    override func setupViews() {
        super.setupViews()
        selectionStyle = .none
        nameLabel.text = "Members (\(membersCount)):"
        textField.removeFromSuperview()
        
        backgroundColor = UIColor.blueButtonColor
        nameLabel.textColor = .white
        accessoryType = .disclosureIndicator
    }

}
